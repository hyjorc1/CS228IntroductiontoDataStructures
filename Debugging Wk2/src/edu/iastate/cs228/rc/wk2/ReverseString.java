package edu.iastate.cs228.rc.wk2;

/**
 *	A class that takes in a word and prints out the reverse of it. 
 *
 * @author Justin Derby
 *
 */
public class ReverseString {

	/**
	 * Returns the reversed string
	 *
	 * @param arr String to reverse
	 * @throws NullPointerException
	 * @return Reversed string
	 */
	public static char[] getReverse(char[] arr)
	{
		// Store the length of the string
		int length = arr.length;
		
		//Reverse it!
		for(int i = 0; i < length; ++i)
		{	
			System.out.format("length=%d, i=%d char=%c\n", length, i, arr[i]);
			char temp = arr[i];
			arr[length - i] = temp;
			arr[i] = arr[length - i];
		}
		
		return arr;
	}
	
}






/*
			arr[length - i] = temp;


 */




//  System.out.format("length=%d, i=%d\n", length, i);
//  System.out.format("length=%d, i=%d, char[i]= %c\n", length, i, (arr[i]);