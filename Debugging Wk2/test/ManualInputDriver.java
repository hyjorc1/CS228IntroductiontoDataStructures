import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import edu.iastate.cs228.rc.wk2.ReverseString;

public class ManualInputDriver {

	@Test
	public void explore() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Waiting for input");
		while (true) {
			String word = scan.next().trim();
			char[] reversed = ReverseString.getReverse(word.toCharArray());
			System.out.println(String.format("input: %s, output: %s", word, new String(reversed)));
			assertFalse(word.contentEquals(new String(reversed)));
		}
	}

	
}
