import static org.junit.Assert.*;

import org.junit.Test;


public class BetterBugs extends Bugs{

	private String fInput;
	private String fExpected;
	
	public String[][] trialData = {
			{"A","A"},
			{"Tom", "moT"}
			
	};
	
	@Test
	public void testAll(){
		for (int i = 0; i < trialData.length; i++){
			trial(trialData[i][0], trialData[i][1]);
		}
	}
}
