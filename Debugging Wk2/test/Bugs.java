import static org.junit.Assert.*;

import org.junit.Test;

import edu.iastate.cs228.rc.wk2.ReverseString;

public class Bugs {

	@Test
	public void bug1_oob(){
		trial("moto","otom");
	}

	@Test
	public void bug2_oob(){
		trial("A","A");
	}
	

	public void trial(String input, String expected){
		char[] reversed = ReverseString.getReverse(input.toCharArray());
		System.out.println(String.format("input: %s, expected: %s, result: %s", input, expected, new String(reversed)));
		assertTrue(input.contentEquals(new String(reversed)));		
	}

}
