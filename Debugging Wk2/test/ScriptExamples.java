import static org.junit.Assert.*;
import org.junit.Test;

import edu.iastate.cs228.rc.wk2.BigProgram;


public class ScriptExamples {

	@Test
	public void simpleNull(){
		String myText = null;

		assertTrue(myText.contentEquals(myText)); 
	}
	
	@Test
	public void propagatedInfection(){
		String myText = "5 Duff Ave.";
		myText = BigProgram.execute(50000, "lines", myText);
		assertTrue("5 Duff Ave.".contentEquals(myText)); 		
	}
	
	@Test
	public void smallPropagation(){
		String myText = "5 Duff Ave.";
		myText = BigProgram.execute(10, "lines", myText);
		assertTrue("5 Duff Ave.".contentEquals(myText)); 		
	}

}
