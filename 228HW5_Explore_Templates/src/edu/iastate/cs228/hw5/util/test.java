package edu.iastate.cs228.hw5.util;

import java.awt.Dimension;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import edu.iastate.cs228.hw5.mapStructures.BorderSegment;
import edu.iastate.cs228.hw5.mapStructures.ConnType;
import edu.iastate.cs228.hw5.mapStructures.Coordinate;
import edu.iastate.cs228.hw5.mapStructures.ParseErrorException;
import edu.iastate.cs228.hw5.mapStructures.Path;
import edu.iastate.cs228.hw5.mapStructures.TerrainType;
import edu.iastate.cs228.hw5.rendering.TerrainBoard;
import edu.iastate.cs228.hw5.rendering.TerrainGraph;

public class test {

	public static void main(String[] args) throws ParseErrorException {
		// TODO Auto-generated method stub
		try {
			int h = 15;
			int w = 20;
			Coordinate.setGeometry(w, h);
			TerrainGraph g = new TerrainGraph(Coordinate.getWidth(), Coordinate.getHeight());
			String s = "3 5 4 5 4 4 4 ";
			NoiseFilterReader rdr = new NoiseFilterReader(new StringReader(s));
			TerrainScanner scan = new TerrainScanner(rdr);
			List<BorderSegment> segs = new ArrayList<BorderSegment>();
			// while (scan.hasNextBorderSegment())
			// segs.add(scan.nextBorderSegment());
			// if (scan.hasNextInt())
			// throw new ParseErrorException("2");
			// g.setBorderEdges(segs, ConnType.wall);

//			if (!scan.hasNextCoord()) {
//				throw new NoSuchElementException("");
//			}
//			Coordinate c1 = scan.nextCoord();
//			if (!scan.hasNextCoord()) {
//				throw new NoSuchElementException("");
//			}
//			Path path = new Path(c1, scan.nextCoord());
//			while (scan.hasNextCoord()) {
//				path.add(scan.nextCoord());
//			}
//			if (scan.hasNextInt())
//				throw new ParseErrorException("");
//			g.setConnectedPath(path, ConnType.hwy2);
			
			List<Coordinate> coords = new ArrayList<Coordinate>();
			while (scan.hasNextCoord())
				coords.add(scan.nextCoord());
			if (scan.hasNextInt())
				throw new ParseErrorException("");
			try {
				g.setTerrain(coords, TerrainType.kBrush);
			} catch (Exception e) {
				throw new ParseErrorException("");
			}

			TerrainBoard m = new TerrainBoard(g);
			m.setPreferredSize(new Dimension(w * 144, h * 144));
			JFrame f = new JFrame();
			f.setSize(500, 500);
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JScrollPane jsp = new JScrollPane(m);
			f.setContentPane(jsp);
			f.setVisible(true);
		} catch (Exception e) {
			throw new ParseErrorException("3");
		}
	}
}
