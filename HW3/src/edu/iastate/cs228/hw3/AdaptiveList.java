package edu.iastate.cs228.hw3;
/*
 *  @author Yijia Huang
 *
 *  An implementation of List<E> based on a doubly-linked list with an array for indexed reads/writes
 *
 */

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * 
 * @author hyj
 * 
 *         An implementation of List<E> based on a doubly-linked list with an
 *         array for indexed reads/writes
 * 
 */
public class AdaptiveList<E> implements List<E> {

	/**
	 * Node in the doubly-linked list
	 */
	class ListNode // private member of outer class
	{
		public E data; // public members:
		public ListNode link; // used outside the inner class
		public ListNode prev; // used outside the inner class

		public ListNode(E item) {
			data = item;
			link = prev = null;
		}
	}

	/**
	 * dummy node made public for testing.
	 */
	public ListNode head; // dummy node made public for testing.

	/**
	 * dummy node made public for testing.
	 */
	public ListNode tail; // dummy node made public for testing.

	/**
	 * number of data items
	 */
	private int numItems; // number of data items

	/**
	 * true if the linked list is up-to-date.
	 */
	private boolean linkedUTD; // true if the linked list is up-to-date.

	/**
	 * the array for storing elements
	 */
	public E[] theArray; // the array for storing elements

	/**
	 * true if the array is up-to-date.
	 */
	private boolean arrayUTD; // true if the array is up-to-date.

	/**
	 * Constructor of AdaptiveList
	 */
	public AdaptiveList() {
		clear();
	}

	/**
	 * Removes all of the elements from this list (optional operation).
	 * 
	 */
	@Override
	public void clear() {
		head = new ListNode(null);
		tail = new ListNode(null);
		head.link = tail;
		tail.prev = head;
		numItems = 0;
		linkedUTD = true;
		arrayUTD = false;
		theArray = null;
	}

	/**
	 * true if the linked list is up-to-date.
	 * 
	 * @return true if the linked list is up-to-date.
	 */
	public boolean getlinkedUTD() {
		return linkedUTD;
	}

	/**
	 * true if the array is up-to-date.
	 * 
	 * @return true if the array is up-to-date.
	 */
	public boolean getarrayUTD() {
		return arrayUTD;
	}

	/**
	 * Constructor of AdaptiveList with collection parameter
	 * 
	 * @param c
	 *            add the collection to the current AdaptiveList
	 */
	public AdaptiveList(Collection<? extends E> c) {
		// TODO
		this();
		for (E e : c)
			add(e);
	}

	/**
	 * Removes the node from the linked list. This method should be used to
	 * remove a node from the linked list.
	 * 
	 * @param toRemove
	 *            The node needed to be removed
	 */

	private void unlink(ListNode toRemove) {
		if (toRemove == head || toRemove == tail)
			throw new RuntimeException("An attempt to remove head or tail");
		toRemove.prev.link = toRemove.link;
		toRemove.link.prev = toRemove.prev;
	}

	/**
	 * Inserts new node toAdd right after old node current. This method should
	 * be used to add a node to the linked list.
	 * 
	 * @param current
	 *            the current node
	 * @param toAdd
	 *            The node needed to be added
	 */
	private void link(ListNode current, ListNode toAdd) {
		if (current == tail)
			throw new RuntimeException("An attempt to link after tail");
		if (toAdd == head || toAdd == tail)
			throw new RuntimeException("An attempt to add head/tail as a new node");
		toAdd.link = current.link;
		toAdd.link.prev = toAdd;
		toAdd.prev = current;
		current.link = toAdd;
	}

	/**
	 * makes theArray up-to-date.
	 */
	private void updateArray() // makes theArray up-to-date.
	{
		if (numItems < 0)
			throw new RuntimeException("numItems is negative: " + numItems);
		if (!linkedUTD)
			throw new RuntimeException("linkedUTD is false");
		// TODO
		// if (isEmpty())
		// throw new RuntimeException("doubly linked list is empty");
		theArray = (E[]) new Object[numItems];
		ListNode cur = head;
		for (int i = 0; i < numItems; i++) {
			theArray[i] = cur.link.data;
			cur = cur.link;
		}
		arrayUTD = true;
	}

	/**
	 * makes the linked list up-to-date.
	 */
	private void updateLinked() // makes the linked list up-to-date.
	{
		if (numItems < 0)
			throw new RuntimeException("numItems is negative: " + numItems);
		if (!arrayUTD)
			throw new RuntimeException("arrayUTD is false");

		if (theArray == null || theArray.length < numItems)
			throw new RuntimeException("theArray is null or shorter");
		// TODO
		head.link = tail;
		tail.prev = head;
		ListNode cur = head;
		for (int i = 0; i < numItems; i++) {
			link(cur, new ListNode(theArray[i]));
			cur = cur.link;
		}
		linkedUTD = true;
	}

	/**
	 * Returns the number of elements in this list.
	 * 
	 * @return the number of elements in this list
	 */
	@Override
	public int size() {
		// TODO
		if (!linkedUTD)
			updateLinked();
		return numItems;
	}

	/**
	 * Returns true if this list contains no elements.
	 * 
	 * @param true
	 *            if this list contains no elements
	 */
	@Override
	public boolean isEmpty() {
		// TODO
		if (!linkedUTD)
			updateLinked();
		return head.link == tail && tail.prev == head;
	}

	/**
	 * Appends the specified element to the end of this list (optional
	 * operation).
	 * 
	 * @param obj
	 *            element to be appended to this list
	 * 
	 * @return true (as specified by Collection.add(E))
	 */
	@Override
	public boolean add(E obj) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		ListNode toAdd = new ListNode(obj);
		link(tail.prev, toAdd);
		numItems++;
		arrayUTD = false;
		return true;
	}

	/**
	 * Appends all of the elements in the specified collection to the end of
	 * this list, in the order that they are returned by the specified
	 * collection's iterator (optional operation).
	 * 
	 * @param c
	 *            collection containing elements to be added to this list
	 * @return true if this list changed as a result of the call
	 *
	 */
	@Override
	public boolean addAll(Collection<? extends E> c) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		if (c.isEmpty())
			return false;
		// for (E e : c)
		// add(e);
		Iterator<? extends E> iter = c.iterator();
		int j = c.size();
		for (int i = 0; i < j; i++)
			add(iter.next());
		arrayUTD = false;
		return true;
	} // addAll 1

	/**
	 * Removes the first occurrence of the specified element from this list, if
	 * it is present (optional operation). If this list does not contain the
	 * element, it is unchanged.
	 * 
	 * @param obj
	 *            element to be removed from this list, if present
	 * @return true if this list contained the specified element
	 */
	@Override
	public boolean remove(Object obj) {
		// TODO
		if (!linkedUTD)
			updateLinked(); 
		if (isEmpty())
			return false;
		ListNode cur;
		for (cur = head.link; cur != tail; cur = cur.link)
			if (obj == cur.data || obj != null && obj.equals(cur.data))
				break;
		if (cur == tail)
			return false;
		unlink(cur);
		numItems--;
		arrayUTD = false;
		return true;
	}

	/**
	 * check if the index is out of bounds
	 * 
	 * @param pos
	 *            the index position needed to be checked
	 */
	private void checkIndex(int pos) // a helper method
	{
		if (pos >= numItems || pos < 0)
			throw new IndexOutOfBoundsException("Index: " + pos + ", Size: " + numItems);
	}

	/**
	 * check if the index is out of bounds
	 * 
	 * @param pos
	 *            the index position needed to be checked
	 */
	private void checkIndex2(int pos) // a helper method
	{
		if (pos > numItems || pos < 0)
			throw new IndexOutOfBoundsException("Index: " + pos + ", Size: " + numItems);
	}

	/**
	 * check if the node is null or tail
	 * 
	 * @param cur
	 *            the current node
	 */
	private void checkNode(ListNode cur) // a helper method
	{
		if (cur == null || cur == tail)
			throw new RuntimeException("numItems: " + numItems + " is too large");
	}

	/**
	 * find the node with the specific position
	 * 
	 * @param pos
	 *            index of node needed to find
	 * @return the desired node
	 */
	private ListNode findNode(int pos) // a helper method
	{
		ListNode cur = head;
		for (int i = 0; i < pos; i++) {
			checkNode(cur);
			cur = cur.link;
		}
		checkNode(cur);
		return cur;
	}

	/**
	 * Inserts the specified element at the specified position in this list
	 * (optional operation). Shifts the element currently at that position (if
	 * any) and any subsequent elements to the right.
	 * 
	 * @param pos
	 *            index at which the specified element is to be inserted
	 * @param obj
	 *            element to be inserted
	 */
	@Override
	public void add(int pos, E obj) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		checkIndex2(pos);
		ListNode toAdd = new ListNode(obj);
		ListNode cur = findNode(pos);
		link(cur, toAdd);
		arrayUTD = false;
		numItems++;
	}

	/**
	 * Inserts all of the elements in the specified collection into this list at
	 * the specified position (optional operation). Shifts the element currently
	 * at that position (if any) and any subsequent elements to the right
	 * (increases their indices).
	 * 
	 * @param pos
	 *            index at which to insert the first element from the specified
	 *            collection
	 * @param c
	 *            collection containing elements to be added to this list
	 * @return true if this list changed as a result of the call
	 */
	@Override
	public boolean addAll(int pos, Collection<? extends E> c) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		checkIndex2(pos);
		if (c.isEmpty())
			return false;
		// int i = pos;
		// for (E e : c)
		// add(i++, e);
		Iterator<? extends E> iter = c.iterator();
		int p = pos;
		int j = c.size();
		for (int i = 0; i < j; i++)
			add(p++, iter.next());
		arrayUTD = false;
		return true;
	} // addAll 2

	/**
	 * Removes the element at the specified position in this list (optional
	 * operation). Shifts any subsequent elements to the left (subtracts one
	 * from their indices). Returns the element that was removed from the list.
	 * 
	 * @param pos
	 *            the index of the element to be removed
	 * @return the element previously at the specified position
	 */
	@Override
	public E remove(int pos) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		checkIndex(pos);
		ListNode cur = findNode(pos);
		ListNode tmp = cur.link;
		unlink(cur.link);
		numItems--;
		arrayUTD = false;
		return tmp.data; // may need to be revised.
	}

	/**
	 * Returns the element at the specified position in this list.
	 * 
	 * @param pos
	 *            index of the element to return
	 * @return the element at the specified position in this list
	 */
	@Override
	public E get(int pos) {
		// TODO
		checkIndex(pos);
		if (!arrayUTD)
			updateArray();
		return theArray[pos];
	}

	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element (optional operation).
	 * 
	 * @param pos
	 *            index of the element to replace
	 * @param obj
	 *            element to be stored at the specified position
	 * @return the element previously at the specified position
	 */
	@Override
	public E set(int pos, E obj) {
		// TODO
		checkIndex(pos);
		if (!arrayUTD)
			updateArray();
		E returnVal = theArray[pos];
		if (!theArray[pos].equals(obj)) {
			theArray[pos] = obj;
			linkedUTD = false;
		}
		return returnVal;
	}

	/**
	 * Returns true if this list contains the specified element. More formally,
	 * returns true if and only if this list contains at least one element e.
	 * 
	 * @param obj
	 *            element whose presence in this list is to be tested
	 * @return true if this list contains the specified element
	 */
	@Override
	public boolean contains(Object obj) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		ListNode cur;
		for (cur = head.link; cur != tail; cur = cur.link)
			if (obj == cur.data || obj != null && obj.equals(cur.data))
				return true;
		return false;
	}

	/**
	 * Returns true if this list contains all of the elements of the specified
	 * collection.
	 * 
	 * @param c
	 *            collection to be checked for containment in this list
	 * @return true if this list contains all of the elements of the specified
	 *         collection
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		for (Object o : c)
			if (!contains(o))
				return false;
		return true;
	} // containsAll

	/**
	 * Returns the index of the last occurrence of the specified element in this
	 * list, or -1 if this list does not contain the element. More formally,
	 * returns the lowest index i.
	 * 
	 * @param obj
	 *            element to search for
	 * @return the index of the first occurrence of the specified element in
	 *         this list, or -1 if this list does not contain the element
	 */
	@Override
	public int indexOf(Object obj) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		ListNode cur;
		int pos = 0;
		for (cur = head.link; cur != tail; cur = cur.link, pos++)
			if (obj == cur.data || obj != null && obj.equals(cur.data))
				return pos;
		return -1;
	}

	/**
	 * Returns the index of the last occurrence of the specified element in this
	 * list, or -1 if this list does not contain the element. More formally,
	 * returns the highest index i.
	 * 
	 * @param obj
	 *            element to search for
	 * @return the index of the last occurrence of the specified element in this
	 *         list, or -1 if this list does not contain the element
	 */
	@Override
	public int lastIndexOf(Object obj) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		ListIterator<E> iter = listIterator(numItems);
		while (iter.hasPrevious()) {
			E data = iter.previous();
			if (obj == data || obj != null && obj.equals(data))
				return iter.nextIndex();
		}
		return -1;
	}

	/**
	 * Removes from this list all of its elements that are contained in the
	 * specified collection (optional operation).
	 * 
	 * @param c
	 *            collection containing elements to be removed from this list
	 * @return true if this list changed as a result of the call
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		if (c == null)
			throw new NullPointerException();
		boolean changed = false;
		ListIterator<E> iter = listIterator();
		while (iter.hasNext()) {
			E data = iter.next();
			if (c.contains(data)) {
				iter.remove();
				arrayUTD = false;
				changed = true;
			}
		}
		return changed;
	}

	/**
	 * Removes from this list all of its elements that are contained in the
	 * specified collection (optional operation).
	 * 
	 * @param c
	 *            collection containing elements to be removed from this list
	 * @return true if this list changed as a result of the call
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		if (c == null)
			throw new NullPointerException();
		boolean changed = false;
		ListIterator<E> iter = listIterator();
		while (iter.hasNext()) {
			E data = iter.next();
			if (!c.contains(data)) {
				iter.remove();
				arrayUTD = false;
				changed = true;
			}
		}
		return changed;
	}

	/**
	 * Returns an array containing all of the elements in this list in proper
	 * sequence (from first to last element).
	 * 
	 * @return an array containing all of the elements in this list in proper
	 *         sequence
	 */
	@Override
	public Object[] toArray() {
		// TODO
		if (!linkedUTD)
			updateLinked();
		Object[] arr = new Object[numItems];
		ListIterator<E> iter = listIterator();
		for (int i = 0; i < numItems; i++)
			arr[i] = iter.next();
		return arr;
	}

	/**
	 * Returns an array containing all of the elements in this list in proper
	 * sequence (from first to last element); the runtime type of the returned
	 * array is that of the specified array. If the list fits in the specified
	 * array, it is returned therein. Otherwise, a new array is allocated with
	 * the runtime type of the specified array and the size of this list.
	 * 
	 * @param arr
	 *            the array into which the elements of this list are to be
	 *            stored, if it is big enough; otherwise, a new array of the
	 *            same runtime type is allocated for this purpose.
	 * @return an array containing the elements of this list
	 */
	@Override
	public <T> T[] toArray(T[] arr) {
		// TODO
		if (!linkedUTD)
			updateLinked();
		if (arr.length < numItems)
			arr = Arrays.copyOf(arr, numItems);
		System.arraycopy(toArray(), 0, arr, 0, numItems);
		if (arr.length > numItems)
			arr[numItems] = null;
		return arr;
	}

	/**
	 * Returns a view of the portion of this list between the specified
	 * fromIndex, inclusive, and toIndex, exclusive. (If fromIndex and toIndex
	 * are equal, the returned list is empty.) The returned list is backed by
	 * this list, so non-structural changes in the returned list are reflected
	 * in this list, and vice-versa. The returned list supports all of the
	 * optional list operations supported by this list.
	 * 
	 * @param fromPos
	 *            low endpoint (inclusive) of the subList
	 * @param toPos
	 *            high endpoint (exclusive) of the subList
	 * @return a view of the specified range within this list
	 */
	@Override
	public List<E> subList(int fromPos, int toPos) {
		throw new UnsupportedOperationException();
	}

	/**
	 * An iterator for lists that allows the programmer to traverse the list in
	 * either direction, modify the list during iteration, and obtain the
	 * iterator's current position in the list. A ListIterator has no current
	 * element; its cursor position always lies between the element that would
	 * be returned by a call to previous() and the element that would be
	 * returned by a call to next().
	 */
	private class AdaptiveListIterator implements ListIterator<E> {
		private int index; // index of next node;
		private ListNode cur; // node at index - 1
		private ListNode last; // node last visited by next() or previous()

		/**
		 * Constructor of AdaptiveListIterator
		 */
		public AdaptiveListIterator() {
			if (!linkedUTD)
				updateLinked();
			// TODO
			cur = last = head;
			index = 0;
		}

		/**
		 * Constructor of AdaptiveListIterator with index pos
		 * 
		 * @param pos
		 *            index of the starting iterator
		 */
		public AdaptiveListIterator(int pos) {
			if (!linkedUTD)
				updateLinked();
			// TODO
			checkIndex2(pos);
			index = pos;
			last = null;
			cur = pos == 0 ? head : findNode(pos);
		}

		/**
		 * Returns true if this list iterator has more elements when traversing
		 * the list in the forward direction. (In other words, returns true if
		 * next() would return an element rather than throwing an exception.)
		 * 
		 * @return the next element in the list
		 */
		@Override
		public boolean hasNext() {
			// TODO
			if (!linkedUTD)
				updateLinked();
			return cur.link != tail;
		}

		/**
		 * Returns the next element in the list and advances the cursor
		 * position. This method may be called repeatedly to iterate through the
		 * list, or intermixed with calls to previous() to go back and forth.
		 * (Note that alternating calls to next and previous will return the
		 * same element repeatedly.)
		 * 
		 * @return the next element in the list
		 * 
		 */
		@Override
		public E next() {
			// TODO
			if (!linkedUTD)
				updateLinked();
			if (!hasNext())
				throw new NoSuchElementException();
			if (index >= numItems)
				throw new RuntimeException("index 1");
			index++;
			last = cur = cur.link;
			return cur.data;
		}

		/**
		 * Returns true if this list iterator has more elements when traversing
		 * the list in the reverse direction. (In other words, returns true if
		 * previous() would return an element rather than throwing an
		 * exception.)
		 * 
		 * @return true if the list iterator has more elements when traversing
		 *         the list in the reverse direction
		 */
		@Override
		public boolean hasPrevious() {
			// TODO
			if (!linkedUTD)
				updateLinked();
			return cur != head;
		}

		/**
		 * Returns the previous element in the list and moves the cursor
		 * position backwards. This method may be called repeatedly to iterate
		 * through the list backwards, or intermixed with calls to next() to go
		 * back and forth. (Note that alternating calls to next and previous
		 * will return the same element repeatedly.)
		 * 
		 * @return the previous element in the list
		 */
		@Override
		public E previous() {
			// TODO
			if (!linkedUTD)
				updateLinked();
			if (!hasPrevious())
				throw new NoSuchElementException();
			if (index <= 0)
				throw new RuntimeException("index 2");
			index--;
			last = cur;
			cur = cur.prev;
			return last.data;
		}

		/**
		 * Returns the index of the element that would be returned by a
		 * subsequent call to next(). (Returns list size if the list iterator is
		 * at the end of the list.)
		 * 
		 * @return the index of the element that would be returned by a
		 *         subsequent call to next, or list size if the list iterator is
		 *         at the end of the list
		 */
		@Override
		public int nextIndex() {
			// TODO
			if (!linkedUTD)
				updateLinked();
			return index;
		}

		/**
		 * Returns the index of the element that would be returned by a
		 * subsequent call to previous(). (Returns -1 if the list iterator is at
		 * the beginning of the list.)
		 * 
		 * @return the index of the element that would be returned by a
		 *         subsequent call to previous, or -1 if the list iterator is at
		 *         the beginning of the list
		 */
		@Override
		public int previousIndex() {
			// TODO
			if (!linkedUTD)
				updateLinked();
			return index - 1;
		}

		/**
		 * Removes from the list the last element that was returned by next() or
		 * previous() (optional operation). This call can only be made once per
		 * call to next or previous. It can be made only if add(E) has not been
		 * called after the last call to next or previous.
		 */
		public void remove() {
			// TODO
			if (!linkedUTD)
				updateLinked();
			if (last == null || last == head || last == tail)
				throw new IllegalStateException();
			if (cur == last) {
				if (index <= 0)
					throw new RuntimeException("index 3");
				index--;
			} // update index if cur is the last node
			numItems--;
			if (cur == last)
				cur = cur.prev;
			unlink(last);
			arrayUTD = false;
			last = null;
		}

		/**
		 * Inserts the specified element into the list (optional operation). The
		 * element is inserted immediately before the element that would be
		 * returned by next(), if any, and after the element that would be
		 * returned by previous(), if any. (If the list contains no elements,
		 * the new element becomes the sole element on the list.) The new
		 * element is inserted before the implicit cursor: a subsequent call to
		 * next would be unaffected, and a subsequent call to previous would
		 * return the new element.
		 * 
		 * @param obj
		 *            the element to insert
		 */
		public void add(E obj) {
			// TODO
			if (!linkedUTD)
				updateLinked();
			ListNode toAdd = new ListNode(obj);
			link(cur, toAdd);
			cur = toAdd;
			last = null;
			index++;
			numItems++;
			arrayUTD = false;
		} // add

		/**
		 * Replaces the last element returned by next() or previous() with the
		 * specified element (optional operation). This call can be made only if
		 * neither remove() nor add(E) have been called after the last call to
		 * next or previous.
		 * 
		 * @param obj
		 *            the element with which to replace the last element
		 *            returned by next or previous
		 */
		@Override
		public void set(E obj) {
			// TODO
			if (!linkedUTD)
				updateLinked();
			arrayUTD = false;
			if (last == null || last == head || last == tail)
				throw new IllegalStateException();
			last.data = obj;
			arrayUTD = false;
		} // set
	} // AdaptiveListIterator

	/**
	 * Compares the specified object with this list for equality. Returns true
	 * if and only if the specified object is also a list, both lists have the
	 * same size, and all corresponding pairs of elements in the two lists are
	 * equal.
	 * 
	 * @param obj
	 *            the object to be compared for equality with this list
	 * @return true if the specified object is equal to this list
	 */
	@Override
	public boolean equals(Object obj) {
		if (!linkedUTD)
			updateLinked();
		if ((obj == null) || !(obj instanceof List<?>))
			return false;
		List<?> list = (List<?>) obj;
		if (list.size() != numItems)
			return false;
		Iterator<?> iter = list.iterator();
		for (ListNode tmp = head.link; tmp != tail; tmp = tmp.link) {
			if (!iter.hasNext())
				return false;
			Object t = iter.next();
			if (!(t == tmp.data || t != null && t.equals(tmp.data)))
				return false;
		}
		if (iter.hasNext())
			return false;
		return true;
	} // equals

	/**
	 * Returns a list iterator over the elements in this list (in proper
	 * sequence).
	 * 
	 * @return a list iterator over the elements in this list (in proper
	 *         sequence)
	 */
	@Override
	public Iterator<E> iterator() {
		return new AdaptiveListIterator();
	}

	/**
	 * Returns a list iterator over the elements in this list (in proper
	 * sequence).
	 * 
	 * @return a list iterator over the elements in this list (in proper
	 *         sequence)
	 */
	@Override
	public ListIterator<E> listIterator() {
		return new AdaptiveListIterator();
	}

	/**
	 * Returns a list iterator over the elements in this list (in proper
	 * sequence), starting at the specified position in the list. The specified
	 * index indicates the first element that would be returned by an initial
	 * call to next. An initial call to previous would return the element with
	 * the specified index minus one.
	 * 
	 * @param pos
	 *            index of the first element to be returned from the list
	 *            iterator (by a call to next)
	 * 
	 * @return a list iterator over the elements in this list (in proper
	 *         sequence), starting at the specified position in the list
	 */
	@Override
	public ListIterator<E> listIterator(int pos) {
		checkIndex2(pos);
		return new AdaptiveListIterator(pos);
	}

	/**
	 * Returns the hash code value for this list. The hash code of a list is
	 * defined to be the result of the calculation.
	 * 
	 * @return the hash code value for this list
	 */
	// Adopted from the List<E> interface.
	@Override
	public int hashCode() {
		if (!linkedUTD)
			updateLinked();
		int hashCode = 1;
		for (E e : this)
			hashCode = 31 * hashCode + (e == null ? 0 : e.hashCode());
		return hashCode;
	}

	/**
	 * Convert the array and doubly-linked list to String.
	 * 
	 * @return String of array and doubly-linked list
	 */
	// You should use the toString*() methods to see if your code works as
	// expected.
	@Override
	public String toString() {
		String eol = System.getProperty("line.separator");
		return toStringArray() + eol + toStringLinked();
	}

	/**
	 * Convert the array to String.
	 * 
	 * @return String of array
	 */
	public String toStringArray() {
		String eol = System.getProperty("line.separator");
		StringBuilder strb = new StringBuilder();
		strb.append("A sequence of items from the most recent array:" + eol);
		strb.append('[');
		if (theArray != null)
			for (int j = 0; j < theArray.length;) {
				if (theArray[j] != null)
					strb.append(theArray[j].toString());
				else
					strb.append("-");
				j++;
				if (j < theArray.length)
					strb.append(", ");
			}
		strb.append(']');
		return strb.toString();
	}

	/**
	 * Convert the doubly-linked list to String
	 * 
	 * @return String of the doubly-linked listF
	 */
	public String toStringLinked() {
		return toStringLinked(null);
	}

	/**
	 * Convert the doubly-linked list to String with iterator
	 * 
	 * @param iter
	 *            iterator of the AdaptiveList
	 * @return String of doubly-linked list and iterator
	 */
	// iter can be null.
	public String toStringLinked(ListIterator<E> iter) {
		int cnt = 0;
		int loc = iter == null ? -1 : iter.nextIndex();

		String eol = System.getProperty("line.separator");
		StringBuilder strb = new StringBuilder();
		strb.append("A sequence of items from the most recent linked list:" + eol);
		strb.append('(');
		for (ListNode cur = head.link; cur != tail;) {
			if (cur.data != null) {
				if (loc == cnt) {
					strb.append("| ");
					loc = -1;
				}
				strb.append(cur.data.toString());
				cnt++;

				if (loc == numItems && cnt == numItems) {
					strb.append(" |");
					loc = -1;
				}
			} else
				strb.append("-");

			cur = cur.link;
			if (cur != tail)
				strb.append(", ");
		}
		strb.append(')');
		return strb.toString();
	}
}
