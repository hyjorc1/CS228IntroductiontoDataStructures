package edu.iastate.cs228.hw1;

/*
 * @author Yijia Huang
*/

/**
 * Model of a protein sequence class for the protein sequence with the letters
 * out of B, b, J, j, O, o, U, u, X, x, Z, z
 */
public class ProteinSequence extends Sequence {
	/**
	 * Construct a protein sequence
	 * 
	 * @param psarr
	 *            the the array copy from superclass with the type character
	 *            array
	 */
	public ProteinSequence(char[] psarr) {
		super(psarr);
	}

	/**
	 * The method returns true if the character argument is a letter and not
	 * equal to one of B, b, J, j, O, o, U, u, X, x, Z, z
	 * 
	 * @return true if the character argument is a letter and not equal to one
	 *         of B, b, J, j, O, o, U, u, X, x, Z, z
	 */
	@Override
	public boolean isValidLetter(char aa) {
		switch (aa) {
		case 'B':
			return false;
		case 'b':
			return false;
		case 'J':
			return false;
		case 'j':
			return false;
		case 'O':
			return false;
		case 'o':
			return false;
		case 'U':
			return false;
		case 'u':
			return false;
		case 'X':
			return false;
		case 'x':
			return false;
		case 'Z':
			return false;
		case 'z':
			return false;
		}
		if (super.isValidLetter(aa)) {
			return true;
		} else {
			return false;
		}
	}
}
