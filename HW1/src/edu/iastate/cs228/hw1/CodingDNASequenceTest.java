package edu.iastate.cs228.hw1;

/**
 * @author Yijia Huang
 */

import static org.junit.Assert.*;
import org.junit.Test;

public class CodingDNASequenceTest {

	@Test
	public void TestCodingDNASequence() {

		try {
			String probst2 = new String("TDG");
			CodingDNASequence dnaseqobj = new CodingDNASequence(probst2.toCharArray());
			fail("Invalid sequence letter for class " + dnaseqobj.getClass().getName());
		} catch (RuntimeException e) {
			System.out.println("Invalid sequence letter for class edu.iastate.cs228.hw1.Sequence");
		}
	}

	@Test
	public void TestcheckStartCodon1() {
		String s = "AATGCCAGTCAGCATAGCGTAGACT";
		CodingDNASequence t = new CodingDNASequence(s.toCharArray());
		assertFalse(t.checkStartCodon());
	}

	@Test
	public void TestcheckStartCodon2() {
		String s = "ATGCCTCAATAG";
		CodingDNASequence t = new CodingDNASequence(s.toCharArray());
		assertTrue(t.checkStartCodon());
	}

	@Test
	public void TestcheckStartCodon3() {
		String s = "aTGCCTCAATAG";
		CodingDNASequence t = new CodingDNASequence(s.toCharArray());
		assertTrue(t.checkStartCodon());
	}

	@Test
	public void TestcheckStartCodon4() {
		String s = "aT";
		CodingDNASequence t = new CodingDNASequence(s.toCharArray());
		assertFalse(t.checkStartCodon());
	}

	@Test
	public void Testtranslate1() {
		try {
			String s = "AATGCCAGTCAGCATAGCGTAGACT";
			CodingDNASequence t = new CodingDNASequence(s.toCharArray());
			t.translate();
			fail("No start codon");
		} catch (RuntimeException e) {
			System.out.println("No start codon");
		}
	}

	@Test
	public void Testtranslate2() {
		try {
			String s = "AA";
			CodingDNASequence t = new CodingDNASequence(s.toCharArray());
			t.translate();
			fail("No start codon");
		} catch (RuntimeException e) {
			System.out.println("No start codon");
		}
	}

	@Test
	public void Testtranslate3() {
		String s = "aTGCCTCAATAG";
		CodingDNASequence t = new CodingDNASequence(s.toCharArray());
		char[] t1 = { 'M', 'P', 'Q' };
		assertArrayEquals(t1, t.translate());
	}

	@Test
	public void Testtranslate4() {
		String s = "aTGCCT";
		CodingDNASequence t = new CodingDNASequence(s.toCharArray());
		char[] t1 = { 'M', 'P' };
		assertArrayEquals(t1, t.translate());
	}

	@Test
	public void Testtranslate5() {
		String s = "aTGCCTCA";
		CodingDNASequence t = new CodingDNASequence(s.toCharArray());
		char[] t1 = { 'M', 'P' };
		assertArrayEquals(t1, t.translate());
	}

	// @Test(expected = Exception.class)
	// public void TestgetAminoAcid() {
	// String s = "aTGCCTCA";
	// CodingDNASequence t = new CodingDNASequence(s.toCharArray());
	// char t1 = 'K';
	// assertEquals(t1, t.getAminoAcid("AAA"));
	// }

}
