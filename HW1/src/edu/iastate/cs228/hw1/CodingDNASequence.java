package edu.iastate.cs228.hw1;

/*
 * Model of a coding DNA sequence used to translates a coding sequence to a protein sequence
 * @author Yijia Huang
*/

public class CodingDNASequence extends DNASequence {
	/**
	 * Construct a coding DNA sequence
	 * 
	 * @param cdnaarr
	 *            the the array copy from superclass with the type character
	 *            array
	 */
	public CodingDNASequence(char[] cdnaarr) {
		super(cdnaarr);
	}

	/**
	 * Determine whether the first three characters in the array seqarr are A/a,
	 * T/t, G/g in this order.
	 * 
	 * @return true if the first three characters in the array seqarr are A/a,
	 *         T/t, G/g in this order
	 */
	public boolean checkStartCodon() {
		if (seqLength() < 3) {
			return false;
		}
		if ((getSeq()[0] == 'a' || getSeq()[0] == 'A') && (getSeq()[1] == 'T' || getSeq()[1] == 't')
				&& (getSeq()[2] == 'G' || getSeq()[2] == 'g')) {
			return true;
		}
		return false;
	}

	/**
	 * The method translates a coding sequence to a protein sequence
	 * 
	 * @return the protein sequence in a new character array
	 */
	public char[] translate() {
		if (checkStartCodon() == false) {
			throw new RuntimeException("No start codon");
		}
		String[] codon = new String[seqLength() / 3];
		String pseq = new String("");
		for (int i = 0; i < seqLength() / 3; i++) {
			codon[i] = "" + getSeq()[i * 3] + getSeq()[i * 3 + 1] + getSeq()[i * 3 + 2];
			if (getAminoAcid(codon[i]) != '$') {
				pseq += getAminoAcid(codon[i]);
			}
		}
		return pseq.toCharArray();
	}

	/**
	 * If the string argument codon encodes an amino acid, then the method
	 * returns the character representing the amino acid.
	 * 
	 * @param codon
	 *            The string consists of the three characters in the array
	 *            seqarr
	 * @return the character representing the amino acid
	 */
	private char getAminoAcid(String codon) {
		if (codon == null)
			return '$';
		char aa = '$';
		switch (codon.toUpperCase()) {
		case "AAA":
			aa = 'K';
			break;
		case "AAC":
			aa = 'N';
			break;
		case "AAG":
			aa = 'K';
			break;
		case "AAT":
			aa = 'N';
			break;

		case "ACA":
			aa = 'T';
			break;
		case "ACC":
			aa = 'T';
			break;
		case "ACG":
			aa = 'T';
			break;
		case "ACT":
			aa = 'T';
			break;

		case "AGA":
			aa = 'R';
			break;
		case "AGC":
			aa = 'S';
			break;
		case "AGG":
			aa = 'R';
			break;
		case "AGT":
			aa = 'S';
			break;

		case "ATA":
			aa = 'I';
			break;
		case "ATC":
			aa = 'I';
			break;
		case "ATG":
			aa = 'M';
			break;
		case "ATT":
			aa = 'I';
			break;

		case "CAA":
			aa = 'Q';
			break;
		case "CAC":
			aa = 'H';
			break;
		case "CAG":
			aa = 'Q';
			break;
		case "CAT":
			aa = 'H';
			break;

		case "CCA":
			aa = 'P';
			break;
		case "CCC":
			aa = 'P';
			break;
		case "CCG":
			aa = 'P';
			break;
		case "CCT":
			aa = 'P';
			break;

		case "CGA":
			aa = 'R';
			break;
		case "CGC":
			aa = 'R';
			break;
		case "CGG":
			aa = 'R';
			break;
		case "CGT":
			aa = 'R';
			break;

		case "CTA":
			aa = 'L';
			break;
		case "CTC":
			aa = 'L';
			break;
		case "CTG":
			aa = 'L';
			break;
		case "CTT":
			aa = 'L';
			break;

		case "GAA":
			aa = 'E';
			break;
		case "GAC":
			aa = 'D';
			break;
		case "GAG":
			aa = 'E';
			break;
		case "GAT":
			aa = 'D';
			break;

		case "GCA":
			aa = 'A';
			break;
		case "GCC":
			aa = 'A';
			break;
		case "GCG":
			aa = 'A';
			break;
		case "GCT":
			aa = 'A';
			break;

		case "GGA":
			aa = 'G';
			break;
		case "GGC":
			aa = 'G';
			break;
		case "GGG":
			aa = 'G';
			break;
		case "GGT":
			aa = 'G';
			break;

		case "GTA":
			aa = 'V';
			break;
		case "GTC":
			aa = 'V';
			break;
		case "GTG":
			aa = 'V';
			break;
		case "GTT":
			aa = 'V';
			break;

		case "TAA":
			aa = '$';
			break;
		case "TAC":
			aa = 'Y';
			break;
		case "TAG":
			aa = '$';
			break;
		case "TAT":
			aa = 'Y';
			break;

		case "TCA":
			aa = 'S';
			break;
		case "TCC":
			aa = 'S';
			break;
		case "TCG":
			aa = 'S';
			break;
		case "TCT":
			aa = 'S';
			break;

		case "TGA":
			aa = '$';
			break;
		case "TGC":
			aa = 'C';
			break;
		case "TGG":
			aa = 'W';
			break;
		case "TGT":
			aa = 'C';
			break;

		case "TTA":
			aa = 'L';
			break;
		case "TTC":
			aa = 'F';
			break;
		case "TTG":
			aa = 'L';
			break;
		case "TTT":
			aa = 'F';
			break;
		default:
			aa = '$';
			break;
		}
		return aa;
	}
}
