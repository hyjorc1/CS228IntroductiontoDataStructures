package edu.iastate.cs228.hw1;

/*
 * @author Yijia Huang
*/

/**
 * Model of the DNA sequence class for the NDA sequence with letters
 * a,A,t,T,c,C,g,G
 */
public class DNASequence extends Sequence {
	/**
	 * Construct the DNA sequence with a character array.
	 * 
	 * @param dnaarr
	 *            the the array copy from superclass with the type character
	 *            array
	 */
	public DNASequence(char[] dnaarr) {
		super(dnaarr);
	}

	/**
	 * Determines whether the character is equal to one of a,A,t,T,c,C,g,G.
	 * 
	 * @param let
	 *            the character needed to be determined
	 * @return true if the character is equal to one of a,A,t,T,c,C,g,G
	 */
	@Override
	public boolean isValidLetter(char let) {
		switch (let) {
		case 'a':
			return true;
		case 'A':
			return true;
		case 'c':
			return true;
		case 'C':
			return true;
		case 'g':
			return true;
		case 'G':
			return true;
		case 't':
			return true;
		case 'T':
			return true;
		}
		return false;
	}

}
