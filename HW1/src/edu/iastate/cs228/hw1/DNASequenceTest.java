package edu.iastate.cs228.hw1;

/**
 * @author Yijia Huang
 */
import static org.junit.Assert.*;
import org.junit.Test;

public class DNASequenceTest {

	String s = "AATGCCAGTCAGCATAGCGTAGACT";
	DNASequence t = new DNASequence(s.toCharArray());

	@Test
	public void TestDNASequence() {
		try {
			String probst2 = new String("TDG");
			DNASequence dnaseqobj = new DNASequence( probst2.toCharArray() );
			fail("Invalid sequence letter for class " +dnaseqobj.getClass().getName());
		} catch (RuntimeException e) {
            System.out.println("Invalid sequence letter for class edu.iastate.cs228.hw1.Sequence");
		}
	}
	
	@Test
	public void TestseqLength() {
		assertTrue(t.seqLength() == 25);
	}

	@Test
	public void TestgetSeq() {
		assertArrayEquals(t.getSeq(), s.toCharArray());
	}

	@Test
	public void TesttoString() {
		assertEquals(t.toString(), s);
	}

	@Test
	public void Testequals1() {
		char[] t2 = s.toCharArray();
		assertFalse(t.equals(t2));
	}

	@Test
	public void Testequals2() {
		String t2 = new String(s);
		assertFalse(t.equals(t2));
	}

	@Test
	public void Testequals3() {
		Sequence t2 = new Sequence(s.toCharArray());
		assertFalse(t2.equals(t));
	}

	@Test
	public void Testequals4() {
		DNASequence t2 = new DNASequence(s.toCharArray());
		assertTrue(t.equals(t2));
		
	}

	@Test
	public void Testequals5() {
		Sequence t2 = new DNASequence(s.toCharArray());
		assertTrue(t.equals(t2));
	}

	@Test
	public void Testequals6() {
		Object t2 = new DNASequence(s.toCharArray());
		assertTrue(t.equals(t2));
	}

	@Test
	public void Testequals7() {
		Object t2 = new CodingDNASequence(s.toCharArray());
		assertFalse(t.equals(t2));
	}
	

	@Test
	public void TestisValidLetter1() {
		char a = '1';
		assertFalse(t.isValidLetter(a));
	}

	@Test
	public void TestisValidLetter2() {
		char a = 'A';
		assertTrue(t.isValidLetter(a));
	}

	@Test
	public void TestisValidLetter3() {
		char a = 'a';
		assertTrue(t.isValidLetter(a));
	}
	
	@Test
	public void TestisValidLetter4() {
		char a = 'f';
		assertFalse(t.isValidLetter(a));
	}

}
