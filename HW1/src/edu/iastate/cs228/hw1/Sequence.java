package edu.iastate.cs228.hw1;

/**
 * Model of the sequence class for the letter sequence
 * 
 * @author Yijia Huang
 */

public class Sequence {
	/**
	 * The sequence with a character array
	 */
	protected char[] seqarr;

	/**
	 * Construct the sequence with a character array.
	 * 
	 * @param sarr
	 * 
	 *            the array copy of seqarr with the type character array
	 */
	public Sequence(char[] sarr) {
		seqarr = new char[sarr.length];
		for (int i = 0; i < (sarr.length); i++) {
			if (isValidLetter(sarr[i])) {
				seqarr[i] = sarr[i];
			} else {
				throw new IllegalArgumentException(
						"Invalid sequence letter for class " + this.getClass().getName());
			}
		}
	}

	/**
	 * Return the length of the character array seqarr.
	 * 
	 * @return the length of the character array seqarr
	 */

	public int seqLength() {
		return seqarr.length;
	}

	/**
	 * Make and return a copy of the char array seqarr.
	 * 
	 * @return a copy of the char array seqarr
	 */
	public char[] getSeq() {
		char[] seq = new char[seqarr.length];
		for (int i = 0; i < seq.length; i++) {
			seq[i] = seqarr[i];
		}
		return seq;
	}

	/**
	 * Return the string representation of the char array seqarr.
	 * 
	 * @return the string representation of the char array seqarr
	 */
	public String toString() {
		String sseq = new String("");
		for (int i = 0; i < seqarr.length; i++) {
			sseq += seqarr[i];
		}
		return sseq;
	}

	/**
	 * Determines whether the obj is not null and equal to the current object.
	 * 
	 * @return true if the obj is not null and equal to the current object
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;// A fast comparison
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			Sequence test = (Sequence) obj;
			return test.toString().equalsIgnoreCase(this.toString());
		}
		return false;
	}

	/**
	 * Determines whether the character is an uppercase or lowercase.
	 * 
	 * @param let
	 *            the character needed to be determined
	 * @return true if the character is an uppercase or lowercase
	 */
	public boolean isValidLetter(char let) {

		return Character.isUpperCase(let) || Character.isLowerCase(let);

	}

}
