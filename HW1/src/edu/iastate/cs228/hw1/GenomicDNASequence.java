package edu.iastate.cs228.hw1;

/*
 * @author Yijia Huang
 * 
 * */

/**
 * Mode of the genomic DNA class used to produce the coding sequence by the DNA
 * sequence
 */
public class GenomicDNASequence extends DNASequence {
	/**
	 * coding border
	 */
	private boolean[] iscoding;

	/**
	 * Construct a Genomic DNA sequence with a character array and save its
	 * reference to a boolean array.
	 * 
	 * @param gdnaarr
	 *            the the array copy from superclass with the type character
	 *            array
	 */
	public GenomicDNASequence(char[] gdnaarr) {
		super(gdnaarr);
		iscoding = new boolean[gdnaarr.length];
		for (int i = 0; i < gdnaarr.length; i++) {
			iscoding[i] = false;
		}
	}

	/**
	 * Make coding mark correspond to the genomic DNA sequence
	 * 
	 * @param first
	 *            the first marked index corresponding to character array seqarr
	 * @param last
	 *            the last marked index corresponding to character array seqarr
	 */
	public void markCoding(int first, int last) {
		if (first > last || first < 0 || last >= this.seqLength()) {
			throw new IllegalArgumentException("Coding border is out of bound");
		} else {
			int i = first;
			while (i < last + 1) {
				iscoding[i] = true;
				i++;
			}
		}
	}

	/**
	 * Return Coding DNA sequence concatenated by all the coding exons.
	 * 
	 * @param exonpos
	 *            used to specify the start and end positions of every coding
	 *            exon in the genomic sequence.
	 * @return coding DNA sequence concatenated by all the coding exons
	 */
	public char[] extractExons(int[] exonpos) {
		if (exonpos.length == 0 || exonpos.length % 2 == 1) {
			throw new IllegalArgumentException("Empty array or odd number of array elements");
		}
		for (int i = 0; i < exonpos.length; i++) {
			if (exonpos[i] < 0 || (exonpos[i] >= this.seqLength())) {
				throw new IllegalArgumentException("Exon position is out of bound");
			}
		}
		for (int i = 0; i < exonpos.length - 1; i++) {
			if (exonpos[i] > exonpos[i + 1]) {
				throw new IllegalArgumentException("Exon positions are not in order");
			}
		}
		for (int i = exonpos[0]; i < exonpos[exonpos.length - 1] + 1; i++) {
			if (iscoding[i] == false) {
				throw new IllegalArgumentException("Noncoding position is found");
			}
		}
		String CodeingDNAseq = "";
		for (int i = 0; i < exonpos.length; i += 2) {
			int j = exonpos[i];
			while (j <= exonpos[i + 1]) {
				CodeingDNAseq += this.toString().charAt(j);
				j++;
			}
		}

		return CodeingDNAseq.toCharArray();

	}

}
