package edu.iastate.cs228.hw1;

/**
 * @author Yijia Huang
 */

import static org.junit.Assert.*;
import org.junit.Test;

public class GenomicDNASequenceTest {

	String s = "AATGCCAGTCAGCATAGCGTAGACT";
	GenomicDNASequence t = new GenomicDNASequence(s.toCharArray());

	// @Test
	// public void testGenomicDNASequence1() {
	// boolean[] a = new boolean[s.length()];
	// for(int i=0; i<s.length(); i++){
	// a[i] = false;
	// }
	// assertArrayEquals(a,t.iscoding);
	// }

	@Test
	public void testGenomicDNASequence2() {
		try {
			String probst3 = new String("TGCH");
			GenomicDNASequence gdnaobj = new GenomicDNASequence(probst3.toCharArray());
			fail("Invalid sequence letter for class " + gdnaobj.getClass().getName());
		} catch (IllegalArgumentException e) {
			System.out.println("Invalid sequence letter for class edu.iastate.cs228.hw1.Sequence");
		}
	}

	@Test
	public void TestmarkCoding1() {
		try {
			t.markCoding(2, 1);
			fail("Coding border is out of bound");
		} catch (IllegalArgumentException e) {
			System.out.println("Coding border is out of bound");
		}
	}

	@Test
	public void TestmarkCoding2() {
		try {
			t.markCoding(-1, 1);
			fail("Coding border is out of bound");
		} catch (IllegalArgumentException e) {
			System.out.println("Coding border is out of bound");
		}
	}

	@Test
	public void TestmarkCoding3() {
		try {
			t.markCoding(0, 25);
			fail("Coding border is out of bound");
		} catch (IllegalArgumentException e) {
			System.out.println("Coding border is out of bound");
		}
	}

	@Test
	public void TestmarkCoding4() {
		try {
			t.markCoding(0, 26);
			fail("Coding border is out of bound");
		} catch (IllegalArgumentException e) {
			System.out.println("Coding border is out of bound");
		}
	}

	@Test
	public void TestextractExons1() {
		try {
			int[] ex = {};
			t.markCoding(0, 24);
			t.extractExons(ex);
			fail("Empty array or odd number of array elements");
		} catch (IllegalArgumentException e) {
			System.out.println("Empty array or odd number of array elements");
		}
	}

	@Test
	public void TestextractExons2() {
		try {
			int[] ex = { 1, 5, 8, 10, 13 };
			t.markCoding(0, 24);
			t.extractExons(ex);
			fail("Empty array or odd number of array elements");
		} catch (IllegalArgumentException e) {
			System.out.println("Empty array or odd number of array elements");
		}
	}

	@Test
	public void TestextractExons3() {
		try {
			int[] ex = { -1, 5, 8, 10, 13, 16 };
			t.markCoding(0, 24);
			t.extractExons(ex);
			fail("Exon position is out of bound");
		} catch (IllegalArgumentException e) {
			System.out.println("Exon position is out of bound");
		}
	}

	@Test
	public void TestextractExons4() {
		try {
			int[] ex = { 1, 5, 8, 10, 13, 25 };
			t.markCoding(0, 24);
			t.extractExons(ex);
			fail("Exon position is out of bound");
		} catch (IllegalArgumentException e) {
			System.out.println("Exon position is out of bound");
		}
	}

	@Test
	public void TestextractExons5() {
		try {
			int[] ex = { 1, 5, 8, 10, 13, 26 };
			t.markCoding(0, 24);
			t.extractExons(ex);
			fail("Exon position is out of bound");
		} catch (IllegalArgumentException e) {
			System.out.println("Exon position is out of bound");
		}
	}

	@Test
	public void TestextractExons6() {
		try {
			int[] ex = { 5, 1, 8, 10, 13, 16 };
			t.markCoding(0, 24);
			t.extractExons(ex);
			fail("Exon positions are not in order");
		} catch (IllegalArgumentException e) {
			System.out.println("Exon positions are not in order");
		}
	}

	@Test
	public void TestextractExons7() {
		int[] ex = { 1, 5, 5, 10, 13, 16 };
		t.markCoding(0, 24);
		String s1 = "ATGCCCAGTCAATAG";
		char[] a = s1.toCharArray();
		assertArrayEquals(a, t.extractExons(ex));
	}

	@Test
	public void TestextractExons8() {
		try {
			int[] ex = { 1, 5, 8, 10, 16, 13 };
			t.markCoding(0, 24);
			t.extractExons(ex);
			fail("Exon positions are not in order");
		} catch (IllegalArgumentException e) {
			System.out.println("Exon positions are not in order");
		}
	}

	@Test
	public void TestextractExons9() {
		try {
			int[] ex = { 1, 5, 8, 10, 13, 16 };
			t.markCoding(2, 24);
			t.extractExons(ex);
			fail("Noncoding position is found");
		} catch (IllegalArgumentException e) {
			System.out.println("Noncoding position is found");
		}
	}

	@Test
	public void TestextractExons10() {
		try {
			int[] ex = { 1, 5, 8, 10, 13, 16 };
			t.markCoding(1, 15);
			t.extractExons(ex);
			fail("Noncoding position is found");
		} catch (IllegalArgumentException e) {
			System.out.println("Noncoding position is found");
		}
	}

	@Test
	public void TestextractExons11() {
		int[] ex = { 1, 5, 8, 10, 13, 16 };
		t.markCoding(0, 24);
		String s1 = "ATGCCTCAATAG";
		char[] a = s1.toCharArray();
		assertArrayEquals(a, t.extractExons(ex));
	}

	@Test
	public void TestextractExons12() {
		int[] ex = { 1, 5, 8, 10, 13, 16 };
		t.markCoding(1, 16);
		String s1 = "ATGCCTCAATAG";
		char[] a = s1.toCharArray();
		assertArrayEquals(a, t.extractExons(ex));
	}
}
