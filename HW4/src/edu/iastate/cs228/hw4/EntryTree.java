package edu.iastate.cs228.hw4;

import java.util.Arrays;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;

/**
 * @author Yijia Huang
 *
 *         An entry tree class Add Javadoc comments to each method
 */
public class EntryTree<K, V> {
	/**
	 * dummy root node made public for grading
	 */
	protected Node root;

	/**
	 * prefixlen is the largest index such that the keys in the subarray keyarr
	 * from index 0 to index prefixlen - 1 are, respectively, with the nodes on
	 * the path from the root to a node. prefixlen is computed by a private
	 * method shared by both search() and prefix() to avoid duplication of code.
	 */
	protected int prefixlen;

	protected class Node implements EntryNode<K, V> {
		protected Node child; // link to the first child node
		protected Node parent; // link to the parent node
		protected Node prev; // link to the previous sibling
		protected Node next; // link to the next sibling
		protected K key; // the key for this node
		protected V value; // the value at this node

		/**
		 * Constructor of class Node
		 * 
		 * @param aKey
		 *            Each node has a unique key of type K
		 * @param aValue
		 *            Each node has a value of type V
		 */
		public Node(K aKey, V aValue) {
			key = aKey;
			value = aValue;
			child = null;
			parent = null;
			prev = null;
			next = null;
		}

		@Override
		public EntryNode<K, V> parent() {
			// TODO Auto-generated method stub
			return parent;
		}

		@Override
		public EntryNode<K, V> child() {
			// TODO Auto-generated method stub
			return child;
		}

		@Override
		public EntryNode<K, V> next() {
			// TODO Auto-generated method stub
			return next;
		}

		@Override
		public EntryNode<K, V> prev() {
			// TODO Auto-generated method stub
			return prev;
		}

		@Override
		public K key() {
			// TODO Auto-generated method stub
			return key;
		}

		@Override
		public V value() {
			// TODO Auto-generated method stub
			return value;
		}
	}

	/**
	 * Constructor of EntryTree with all null root node
	 */
	public EntryTree() {
		root = new Node(null, null);
	}

	/**
	 * Returns the value of the entry with a specified key sequence, or null if
	 * this tree contains no entry with the key sequence.
	 * 
	 * @param keyarr
	 * @return the value of the entry with a specified key sequence, or null if
	 *         this tree contains no entry with the key sequence.
	 */
	public V search(K[] keyarr) {
		// TODO
		if (keyarr == null || keyarr.length == 0)
			return null;
		for (K e : keyarr)
			if (e == null)
				throw new NullPointerException();
		Node cur = findNode(keyarr);
		if (prefixlen != keyarr.length)
			return null;
		return cur.value;
	}

	/**
	 * The method returns an array of type K[] with the longest prefix of the
	 * key sequence specified in keyarr such that the keys in the prefix label
	 * the nodes on the path from the root to a node. The length of the returned
	 * array is the length of the longest prefix.
	 * 
	 * @param keyarr
	 *            a key sequence
	 * @return The method returns null if keyarr is null, or its length is 0. If
	 *         any element of keyarr is null, then the method throws a
	 *         NullPointerException exception. The method returns an array of
	 *         type K[] with the longest prefix of the key sequence specified in
	 *         keyarr such that the keys in the prefix are, respectively, with
	 *         the nodes on the path from the root to a node.
	 */
	public K[] prefix(K[] keyarr) {
		// TODO
		// Hint: An array of the same type as keyarr can be created with
		// Arrays.copyOf().
		if (keyarr == null || keyarr.length == 0)
			return null;
		for (K e : keyarr)
			if (e == null)
				throw new NullPointerException();
		findNode(keyarr);
		if (prefixlen == 0)
			return null;
		else
			return Arrays.copyOf(keyarr, prefixlen);
	}

	/**
	 * The method locates the node P corresponding to the longest prefix of the
	 * key sequence specified in keyarr such that the keys in the prefix label
	 * the nodes on the path from the root to the node. If the length of the
	 * prefix is equal to the length of keyarr, then the method places aValue at
	 * the node P and returns true. Otherwise, the method creates a new path of
	 * nodes (starting at a node S) labelled by the corresponding suffix for the
	 * prefix, connects the prefix path and suffix path together by making the
	 * node S a child of the node P, and returns true.
	 * 
	 * @param keyarr
	 *            a key sequence
	 * @param aValue
	 *            the value needed to add
	 * @return true if add is completed successfully
	 */
	public boolean add(K[] keyarr, V aValue) {
		// TODO
		if (keyarr == null || keyarr.length == 0 || aValue == null)
			return false;
		for (K e : keyarr)
			if (e == null)
				throw new NullPointerException();
		Node cur = findNode(keyarr);
		if (prefixlen == 0)
			return addNewNodes(root, keyarr, aValue);
		K[] suffix = Arrays.copyOfRange(keyarr, prefixlen, keyarr.length);
		if (suffix.length == 0)
			cur.value = aValue;
		return addNewNodes(cur, suffix, aValue);
	}

	/**
	 * This method is used to creates a new path of nodes and connect it to the
	 * node P corresponding to the longest prefix of the key sequence specified
	 * in keyarr such that the keys in the prefix label the nodes on the path
	 * from the root to the node.
	 * 
	 * @param r
	 *            node P corresponding to the longest prefix of the key sequence
	 *            specified in keyarr
	 * @param keyarr
	 *            a key sequence
	 * @param aValue
	 *            the value needed to add
	 * @return true if add is completed successfully
	 */
	public boolean addNewNodes(Node r, K[] keyarr, V aValue) {
		Node cur = r;
		int pos = 0;
		while (pos < keyarr.length) {
			Node toAdd = new Node(keyarr[pos], null);
			toAdd.parent = cur;
			if (cur.child == null) {
				cur.child = toAdd;
				cur = toAdd;
			} else {
				cur = cur.child;
				while (cur.next != null)
					cur = cur.next;
				cur.next = toAdd;
				toAdd.prev = cur;
				cur = toAdd;
			}
			if (pos == keyarr.length - 1)
				cur.value = aValue;
			pos++;
		}
		return true;
	}

	/**
	 * Find the node P corresponding to the longest prefix of the key sequence
	 * specified in keyarr such that the keys in the prefix label the nodes on
	 * the path from the root to the node.
	 * 
	 * @param keyarr
	 *            a key sequence
	 * @return the node P
	 */
	public Node findNode(K[] keyarr) {
		Node cur = root.child;
		Node last = cur;
		int pos = 0;
		while (pos < keyarr.length) {
			if (cur == null)
				break;
			if (keyarr[pos].equals(cur.key)) {
				last = cur;
				cur = cur.child;
				pos++;
			} else
				cur = cur.next;
		}
		prefixlen = pos;
		return last;
	}

	// /**
	// * Find the index corresponding to the longest prefix of the key sequence
	// * specified in keyarr such that the keys in the prefix label the nodes on
	// * the path from the root to the node.
	// *
	// * @param keyarr
	// * a key sequence
	// * @return the index corresponding to the longest prefix
	// */
	// public int findPos(K[] keyarr) {
	// Node cur = root.child;
	// int pos = 0;
	// while (pos < keyarr.length) {
	// if (cur == null)
	// return pos - 1;
	// if (keyarr[pos].equals(cur.key)) {
	// cur = cur.child;
	// pos++;
	// } else
	// cur = cur.next;
	// }
	// return pos - 1;
	// }

	/**
	 * Removes the entry for a key sequence from this tree and returns its value
	 * if it is present. Otherwise, it makes no change to the tree and returns
	 * null.
	 * 
	 * @param keyarr
	 *            a key sequence
	 * @return the value to be removed
	 */
	public V remove(K[] keyarr) {
		// TODO
		if (keyarr == null || keyarr.length == 0)
			return null;
		for (K e : keyarr)
			if (e == null)
				throw new NullPointerException();
		Node cur = findNode(keyarr);
		if (prefixlen != keyarr.length)
			return null;
		V temp = cur.value;
		cur.value = null;
		while (cur != root)
			if (cur.child == null) {
				if (cur.next != null && cur.prev == null) {
					cur.parent.child = cur.next;
					cur.next.prev = null;
					break;
				} else if (cur.next == null && cur.prev != null) {
					cur.prev.next = null;
					break;
				} else if (cur.next != null && cur.prev != null) {
					cur.prev.next = cur.next;
					cur.next.prev = cur.prev;
					break;
				} else {
					if (cur.value != null)
						break;
					cur = cur.parent;
					cur.child = null;
				}
			} else
				break;
		return temp;
	}

	/**
	 * The method prints the tree on the console in the output format shown in
	 * an example output file.
	 */
	public void showTree() {
		// TODO
		System.out.println(root.key + "->" + root.value);
		recShowTree(root.child, "");
	}

	/**
	 * ShowTree method with recursion
	 * 
	 * @param r
	 *            Node
	 * @param level
	 *            blank space
	 */
	public void recShowTree(Node r, String level) {
		if (r == null)
			return;
		System.out.print("      ");
		System.out.println(level + r.key + "->" + r.value);
		recShowTree(r.child, "  " + level);
		recShowTree(r.next, level);
	}

}
