package edu.iastate.cs228.hw4;

import java.util.Arrays;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;

/**
 * @author Yijia Huang
 * 
 *         An application class
 */
public class Dictionary {
	public static void main(String[] args) throws FileNotFoundException {
		// TODO
		File file = new File("infile.txt");
		Scanner scan = new Scanner(file);
		EntryTree<Character, String> EntryTree = new EntryTree<Character, String>();
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			Scanner LineScan = new Scanner(line);
			while (LineScan.hasNext()) {
				String method = LineScan.next();
				if (method.equals("add")) {
					String StringKeyArr = LineScan.next();
					Character[] CharKeyArr = new Character[StringKeyArr.length()];
					for (int i = 0; i < StringKeyArr.length(); i++)
						CharKeyArr[i] = StringKeyArr.charAt(i);
					String Value = LineScan.next();
					System.out.println("Command: " + method + " " + StringKeyArr + " " + Value);
					System.out.println("Result from an add: " + EntryTree.add(CharKeyArr, Value));
					System.out.println();
				}
				if (method.equals("showTree")) {
					System.out.println("Command: " + method);
					System.out.println();
					System.out.println("Result from a showTree:");
					EntryTree.showTree();
					System.out.println();
				}
				if (method.equals("search")) {
					String StringKeyArr = LineScan.next();
					Character[] CharKeyArr = new Character[StringKeyArr.length()];
					for (int i = 0; i < StringKeyArr.length(); i++)
						CharKeyArr[i] = StringKeyArr.charAt(i);
					System.out.println("Command: " + method + " " + StringKeyArr);
					System.out.println("Result from a search: " + EntryTree.search(CharKeyArr));
					System.out.println();
				}
				if (method.equals("prefix")) {
					String StringKeyArr = LineScan.next();
					Character[] CharKeyArr = new Character[StringKeyArr.length()];
					for (int i = 0; i < StringKeyArr.length(); i++)
						CharKeyArr[i] = StringKeyArr.charAt(i);
					System.out.println("Command: " + method + " " + StringKeyArr);
					Character[] CharPrefix = EntryTree.prefix(CharKeyArr);
					String StringPrefix = "";
					for(Character e:CharPrefix)
						StringPrefix+=e;
					System.out.println("Result from a prefix: " + StringPrefix);
					System.out.println();
				}
				if (method.equals("remove")) {
					String StringKeyArr = LineScan.next();
					Character[] CharKeyArr = new Character[StringKeyArr.length()];
					for (int i = 0; i < StringKeyArr.length(); i++)
						CharKeyArr[i] = StringKeyArr.charAt(i);
					System.out.println("Command: " + method + " " + StringKeyArr);
					System.out.println("Result from a remove: " + EntryTree.remove(CharKeyArr));
					System.out.println();
				}
			}
		}

	}
}
