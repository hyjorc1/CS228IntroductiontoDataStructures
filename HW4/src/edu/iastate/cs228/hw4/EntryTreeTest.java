package edu.iastate.cs228.hw4;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EntryTreeTest {
	EntryTree<Character, String> entrytree = new EntryTree<Character, String>();
	Character[] ca1 = {};
	Character[] ca2 = null;
	Character[] ca3 = { 'a', 'b', 'c', null };
	Character[] ca4 = { 'a', 'b', 'c', 'd', 'e', 'f', 'g' };
	Character[] ca5 = { 'a', 'b'};
	Character[] ca6 = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
	Character[] ca7 = { 'b', 'b', 'c' };
	Character[] ca8 = { 'a', 'b', 'c', 'e', 'f' };
	Character[] ca9 = { 'e', 'b', 'c' };
	Character[] ca10 = { 'a', 'b', 'c' };
	Character[] ca11 = { 'a', 'b', 'c', 'd', 'e' };
	Character[] ca12 = { 'a', 'b', 'c', 'd', 'e','l' };
	Character[] ca13 = { 'a', 'b', 'c','g','f' };

	@Before
	public void Test() {
		entrytree.add(ca4, "lol");
		entrytree.add(ca7, "haha");
		entrytree.add(ca8, "mmmm");
		entrytree.add(ca10, "kkk");
		entrytree.add(ca11, "aaa");
		entrytree.add(ca13, "ddd");
	}

	@Test
	public void SearchTest1() {
		// System.out.println(ca1.length);
		assertEquals(null, entrytree.search(ca1));
	}

	@Test
	public void SearchTest2() {
		// System.out.println(ca2);
		assertEquals(null, entrytree.search(ca2));
	}

	@Test(expected = NullPointerException.class)
	public void SearchTest3() {
		entrytree.search(ca3);
	}

	@Test
	public void SearchTest4() {
		assertEquals(null, entrytree.search(ca5));
	}

	@Test
	public void SearchTest5() {
//		entrytree.showTree();
		assertEquals("lol", entrytree.search(ca4));
	}

	@Test
	public void PrefixTest1() {
		assertEquals(null, entrytree.prefix(ca1));
	}

	@Test
	public void PrefixTest2() {
		assertEquals(null, entrytree.prefix(ca2));
	}

	@Test(expected = NullPointerException.class)
	public void PrefixTest3() {
		entrytree.prefix(ca3);	
	}

	@Test
	public void PrefixTest4() {
		assertEquals(7, entrytree.prefix(ca4).length);	
	}

	@Test
	public void PrefixTest5() {
		assertEquals(2, entrytree.prefix(ca5).length);		
	}

	@Test
	public void PrefixTest6() {
		assertEquals(null, entrytree.prefix(ca9));		
	}

	@Test
	public void AddTest1() {
		assertEquals(false, entrytree.add(ca1, "lol"));		
	}

	@Test
	public void AddTest2() {
		assertEquals(false, entrytree.add(ca2, "lol"));		
	}

	@Test
	public void AddTest3() {
		assertEquals(false, entrytree.add(ca4, null));
	}

	@Test(expected = NullPointerException.class)
	public void AddTest4() {
		entrytree.add(ca3, "ol");
	}
	
	@Test
	public void AddTest5() {
		entrytree.add(ca4, "ol");
		assertEquals("ol", entrytree.search(ca4));
	}
	
	@Test
	public void AddTest6() {
		assertEquals("kkk", entrytree.search(ca10));
	}
	
	@Test
	public void AddTest7() {
		assertEquals("aaa", entrytree.search(ca11));
	}

	@Test
	public void AddTest8() {
		assertEquals(true, entrytree.add(ca4, "lol"));
	}
	
	@Test
	public void RemoveTest1() {
		assertEquals(null, entrytree.remove(ca1));
	}
	
	@Test
	public void RemoveTest2() {
		assertEquals(null, entrytree.remove(ca2));
	}
	
	@Test (expected = NullPointerException.class)
	public void RemoveTest3() {
		entrytree.remove(ca3);
	}
	
	@Test
	public void RemoveTest4() {
		assertEquals(null, entrytree.remove(ca12));
	}
	
	@Test
	public void RemoveTest5() {
		entrytree.remove(ca10);
		assertEquals(null, entrytree.search(ca10));
	}
	
	@Test
	public void RemoveTest6() {
		entrytree.remove(ca8);
//		entrytree.showTree();
		assertEquals(3, entrytree.prefix(ca8).length);
	}
	
	@Test
	public void RemoveTest7() {
		entrytree.remove(ca8);
//		entrytree.showTree();
		assertEquals(null, entrytree.search(ca8));
	}
	
	@Test
	public void RemoveTest8() {
		entrytree.remove(ca13);
//		entrytree.showTree();
		assertEquals(3, entrytree.prefix(ca13).length);
	}
	
	@Test
	public void RemoveTest9() {
		entrytree.remove(ca4);
//		entrytree.showTree();
		assertEquals("aaa", entrytree.search(ca11));
	}
	
	@Test
	public void RemoveTest10() {
		entrytree.remove(ca4);
//		entrytree.showTree();
		assertEquals(null, entrytree.remove(ca4));
	}
	
	@Test
	public void RemoveTest11() {
		entrytree.remove(ca4);
		entrytree.remove(ca11);
		entrytree.showTree();
		assertEquals(new Character('e'), entrytree.root.child.child.child.child.key);
	}
}
